import styled from "@emotion/styled";
import { CardContent, CardMedia, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import { MovieSummary } from "../api/models";
import AnimatedCard from "./styled/AnimatedCard";

type Props = {
  data: MovieSummary;
};

const LinkWithoutUnderline = styled(Link)({
  textDecoration: "none",
});

export default function CardMovieSummary({
  data: { Poster, Title, Type, Year, imdbID },
}: Props) {
  return (
    <LinkWithoutUnderline to={`/movie/${imdbID}`}>
      <AnimatedCard sx={{ flexDirection: "row", display: "flex" }}>
        <CardMedia component="img" image={Poster} sx={{ width: 100 }} />
        <CardContent>
          <Typography variant="h5">{Title}</Typography>
          <Typography>{Year}</Typography>
          <Typography>{Type}</Typography>
        </CardContent>
      </AnimatedCard>
    </LinkWithoutUnderline>
  );
}
