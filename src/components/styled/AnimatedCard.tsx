import styled from "@emotion/styled";
import { Card } from "@mui/material";

export default styled(Card)`
  transition: 0.2s;
  &:hover {
    transform: translateY(-2px);
  }
  &:active {
    transform: scale(0.95);
  }
  user-select: none;
`;
