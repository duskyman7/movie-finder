import React, { useState } from "react";

export const SearchContext = React.createContext<{
  searchValue: string;
  setSearchValue: React.Dispatch<React.SetStateAction<string>>;
}>({
  searchValue: "",
  setSearchValue: () => {},
});

export function SearchContextProvider({ children }: React.PropsWithChildren) {
  const [searchValue, setSearchValue] = useState("");
  return (
    <SearchContext.Provider
      value={{ searchValue, setSearchValue }}
      children={children}
    />
  );
}
