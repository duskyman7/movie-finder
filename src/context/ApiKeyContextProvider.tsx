import React, { useCallback, useMemo, useState } from "react";

export const ApiKeyContext = React.createContext<{
  apiKey: string | null;
  setApiKey: (newApiKey: string | null) => void;
}>({
  apiKey: "",
  setApiKey: () => {},
});

export function ApiKeyContextProvider({ children }: React.PropsWithChildren) {
  const [apiKey, _setApiKey] = useState<string | null>(
    window.localStorage.getItem("apiKey")
  );
  const setApiKey = useCallback((newValue: string | null) => {
    if (newValue === null) {
      window.localStorage.removeItem("apiKey");
    } else {
      window.localStorage.setItem("apiKey", newValue);
    }
    _setApiKey(newValue);
  }, []);
  const value = useMemo(() => ({ apiKey, setApiKey }), [apiKey, setApiKey]);

  return (
    <ApiKeyContext.Provider value={value}>{children}</ApiKeyContext.Provider>
  );
}
