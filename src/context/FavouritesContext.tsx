import React, { useCallback, useEffect, useMemo, useState } from "react";
import { MovieDetail } from "../api/models";

export const FavouritesContext = React.createContext<{
  favourites: MovieDetail[];
  addFavourite: (movieDetail: MovieDetail) => void;
  removeFavourite: (id: string) => void;
}>({
  favourites: [],
  addFavourite: () => {},
  removeFavourite: () => {},
});

const LOCAL_STORAGE_KEY = "favourite_movies";

export function FavouritesContextProvider({
  children,
}: React.PropsWithChildren) {
  const [favourites, setFavourites] = useState<MovieDetail[] | null>(null);

  // effect to initialize favourites list from localstorage
  useEffect(() => {
    const result = window.localStorage.getItem(LOCAL_STORAGE_KEY);
    setFavourites(result != null ? JSON.parse(result) : []);
  }, []);

  const addFavourite = useCallback((movieDetail: MovieDetail) => {
    setFavourites((oldArr) => {
      const newArr = [...(oldArr ?? []), movieDetail];
      window.localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(newArr));
      return newArr;
    });
  }, []);

  const removeFavourite = useCallback((id: string) => {
    setFavourites((oldArr) => {
      const newArr = (oldArr ?? []).filter(({ imdbID }) => id !== imdbID);
      window.localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(newArr));
      return newArr;
    });
  }, []);

  const value = useMemo(
    () => ({
      favourites: favourites ?? [],
      addFavourite,
      removeFavourite,
    }),
    [favourites, addFavourite, removeFavourite]
  );

  if (favourites === null) {
    return null; // wait until we have data parsed from local storage
  }
  return <FavouritesContext.Provider value={value} children={children} />;
}
