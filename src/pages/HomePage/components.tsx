import {
  Box,
  Button,
  CircularProgress,
  Grid,
  Paper,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { MovieSummary } from "../../api/models";
import CardMovieSummary from "../../components/CardMovieSummary";

type PaperSearchProps = {
  searchValue: string;
  setSearchValue: React.Dispatch<React.SetStateAction<string>>;
  refetch: () => void;
};

export function PaperSearch({
  searchValue,
  setSearchValue,
  refetch,
}: PaperSearchProps) {
  return (
    <Paper elevation={4}>
      <Box sx={{ padding: { xs: 1, md: 4 } }}>
        <Stack direction={"row"} spacing={1}>
          <TextField
            fullWidth
            hiddenLabel
            variant="outlined"
            placeholder="Find the movie you are dreaming of"
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
          />
        </Stack>
      </Box>
    </Paper>
  );
}

type SearchResultsProps = {
  data: Array<MovieSummary>;
  searchValue: string;
  showMoreButtonVisible: boolean;
  isFetchingNextPage: boolean;
  fetchNextPage: () => void;
};

export function SearchResults({
  data,
  searchValue,
  showMoreButtonVisible,
  fetchNextPage,
  isFetchingNextPage,
}: SearchResultsProps) {
  return !searchValue ? null : (
    <Box sx={{ padding: { xs: 1, md: 4 } }}>
      <Typography textAlign={"center"} variant="h4">
        Found results:
      </Typography>
      <Grid container>
        {data.map((summary) => (
          <Grid
            item
            key={summary.imdbID}
            xl={6}
            md={6}
            sm={12}
            xs={12}
            sx={{ padding: { xs: 1, md: 2 } }}
          >
            <CardMovieSummary data={summary} />
          </Grid>
        ))}
      </Grid>
      <ShowMorePart
        visible={showMoreButtonVisible}
        isFetchingNextPage={isFetchingNextPage}
        onShowMore={fetchNextPage}
      />
    </Box>
  );
}

type ShowMorePartProps = {
  visible: boolean;
  isFetchingNextPage: boolean;
  onShowMore: () => void;
};

function ShowMorePart({
  visible,
  onShowMore,
  isFetchingNextPage,
}: ShowMorePartProps) {
  if (!visible) {
    return null;
  }
  return (
    <Stack direction={"row"} justifyContent="center" sx={{ marginTop: 4 }}>
      {isFetchingNextPage ? (
        <CircularProgress />
      ) : (
        <Button variant="text" onClick={() => onShowMore()}>
          Load more
        </Button>
      )}
    </Stack>
  );
}
