import {
  Alert,
  AlertColor,
  Box,
  Button,
  CircularProgress,
  Paper,
  Stack,
} from "@mui/material";
import { milliseconds } from "date-fns";
import { useContext, useEffect, useMemo } from "react";
import { useSearchParams } from "react-router-dom";
import { useSearchMovies } from "../../api";
import { SearchContext } from "../../context/SearchContext";
import { PaperSearch, SearchResults } from "./components";

export default function HomePage() {
  const [searchParams] = useSearchParams();
  const searchParam = searchParams.get("search");
  const { searchValue, setSearchValue } = useContext(SearchContext);
  const {
    data,
    error,
    hasNextPage,
    fetchNextPage,
    refetch,
    isStale,
    isFetchingNextPage,
    isLoading,
  } = useSearchMovies(searchValue);

  const movies = useMemo(
    () => (data?.pages ?? []).map(({ Search }) => Search).flat(),
    [data?.pages]
  );

  // effect to optionally initialize searchValue from query param (only if the context value is empty)
  useEffect(() => {
    if (!searchValue && !!searchParam) {
      setSearchValue(searchParam);
    }
    // Intentionally leaving the dependency array empty, because we want this to run only once on mount
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Effect controlling automatic run of query when user stops typing
  useEffect(() => {
    if (!isStale || !searchValue) {
      return; // no need to update, we have the data already
    }
    const id = setTimeout(refetch, milliseconds({ seconds: 1 }));
    return () => {
      clearTimeout(id);
    };
  }, [isStale, refetch, searchValue]);

  const showLoading = searchValue && (isLoading || isStale);

  const knownErrors = useMemo(
    () => [
      {
        error: "Movie not found!",
        message: "No movies found, try something else",
      },
      {
        error: "Too many results.",
        message: "Search is too broad, try something more specific",
      },
    ],
    []
  );

  const alertColor: AlertColor = knownErrors.find(
    (knownError) => knownError.error === error?.message
  )
    ? "info"
    : "error";
  const alertMessage =
    knownErrors.find((knownError) => knownError.error === error?.message)
      ?.message ?? "Something went wrong";

  const isKnownError = !!knownErrors.find(
    (knownError) => knownError.error === error?.message
  );

  return (
    <Stack>
      <PaperSearch
        searchValue={searchValue}
        setSearchValue={setSearchValue}
        refetch={refetch}
      />
      {(data !== undefined || showLoading) && (
        <Box sx={{ mt: 2 }}>
          <Paper elevation={1} sx={{ padding: { xs: 1 } }}>
            {error ? (
              <Alert
                severity={alertColor}
                action={
                  isKnownError ? null : (
                    <Button
                      size="small"
                      variant="text"
                      sx={{ ml: 4 }}
                      onClick={() => refetch()}
                      color="inherit"
                    >
                      Retry
                    </Button>
                  )
                }
              >
                {alertMessage}
              </Alert>
            ) : showLoading ? (
              <Stack direction="row" justifyContent={"center"}>
                <CircularProgress />
              </Stack>
            ) : (
              <SearchResults
                data={movies}
                fetchNextPage={fetchNextPage}
                isFetchingNextPage={isFetchingNextPage}
                showMoreButtonVisible={!!hasNextPage}
                searchValue={searchValue}
              />
            )}
          </Paper>
        </Box>
      )}
    </Stack>
  );
}
