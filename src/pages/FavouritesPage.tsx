import CloseIcon from "@mui/icons-material/Close";
import WhatshotIcon from "@mui/icons-material/Whatshot";
import { Box, Button, Fab, Tooltip, Typography } from "@mui/material";
import { Stack } from "@mui/system";
import { useContext } from "react";
import CardMovieSummary from "../components/CardMovieSummary";
import { FavouritesContext } from "../context/FavouritesContext";
import useNavigateToHome from "../hooks/useNavigateToHome";

export default function FavouritesPage() {
  const { favourites, removeFavourite } = useContext(FavouritesContext);
  const navigateToHome = useNavigateToHome();

  if (favourites.length === 0) {
    return (
      <Stack spacing={4}>
        <Typography variant="h3" textAlign="center">
          You have no favourites yet
        </Typography>
        <Button
          startIcon={<WhatshotIcon />}
          endIcon={<WhatshotIcon />}
          variant="contained"
          size="large"
          onClick={navigateToHome}
        >
          Find something good
        </Button>
      </Stack>
    );
  }

  return (
    <Stack spacing={4} mb={4}>
      <Typography variant="h3" textAlign={"center"}>
        Your favourite movies
      </Typography>
      {favourites.map((favourite) => (
        <Stack key={favourite.imdbID} direction={"row"} alignItems="center">
          <Box flex={1}>
            <CardMovieSummary data={favourite} />
          </Box>
          <Box flex={0} ml={4}>
            <Tooltip title="Remove movie from favourites">
              <Fab
                onClick={() => removeFavourite(favourite.imdbID)}
                size="large"
                color="error"
              >
                <CloseIcon />
              </Fab>
            </Tooltip>
          </Box>
        </Stack>
      ))}
    </Stack>
  );
}
