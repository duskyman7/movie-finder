import {
  Button,
  Card,
  Container,
  Grid,
  Stack,
  styled,
  TextField,
  Typography,
} from "@mui/material";
import { useCallback, useContext, useState } from "react";
import { redirect } from "react-router-dom";
import { ApiKeyContext } from "../context/ApiKeyContextProvider";

const StyledContainer = styled(Container)`
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export default function LoginPage() {
  const { setApiKey } = useContext(ApiKeyContext);
  const [value, setValue] = useState("");
  const onClick = useCallback(() => {
    setApiKey(value);
    redirect("/");
  }, [setApiKey, value]);
  return (
    <StyledContainer fixed>
      <Grid container justifyContent={"center"} alignItems={"center"}>
        <Grid item xs={12} sm={12} md={6} xl={6}>
          <LoginCard value={value} setValue={setValue} onClick={onClick} />
        </Grid>
      </Grid>
    </StyledContainer>
  );
}

type LoginCardProps = {
  value: string;
  setValue: React.Dispatch<React.SetStateAction<string>>;
  onClick: () => void;
};

function LoginCard({ value, setValue, onClick }: LoginCardProps) {
  return (
    <Card sx={{ padding: 2 }}>
      <Stack direction="column" spacing={8}>
        <Typography variant="h3" textAlign={"center"}>
          "Login" screen
        </Typography>
        <TextField
          label="Enter API key"
          value={value}
          onChange={(e) => setValue(e.target.value)}
        />
        <Button
          disabled={!value}
          size="large"
          onClick={onClick}
          color="primary"
          variant="contained"
        >
          Save API key
        </Button>
      </Stack>
    </Card>
  );
}
