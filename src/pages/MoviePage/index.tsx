import {
  Alert,
  Button,
  CircularProgress,
  Stack,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import { useNavigate, useParams } from "react-router-dom";
import { useGetMovieDetails } from "../../api";
import {
  AdditionalInfoTable,
  AddToFavouritesButton,
  Header,
} from "./components";
import {
  PaperWithBackground,
  TransparentBackgroundBox,
} from "./styledComponents";

export default function MoviePage() {
  const { id } = useParams();
  return !id ? <IdNotFound /> : <MoviePageComponent id={id} />;
}

function IdNotFound() {
  const navigate = useNavigate();
  return (
    <Alert
      severity="error"
      action={
        <Button
          size="small"
          variant="text"
          sx={{ ml: 4 }}
          onClick={() => navigate(-1)}
          color="inherit"
        >
          Go back
        </Button>
      }
    >
      Sorry, given movie does not seem to exist!
    </Alert>
  );
}

type MoviePageComponentProps = {
  id: string;
};

function MoviePageComponent({ id }: MoviePageComponentProps) {
  const { data, error, isLoading } = useGetMovieDetails(id);

  if (isLoading) {
    return (
      <Stack flexDirection={"column"} alignItems="center">
        <CircularProgress />
      </Stack>
    );
  }

  if (error || !data) {
    return <IdNotFound />;
  }

  return (
    <PaperWithBackground elevation={4} backgroundImageUrl={data.Poster}>
      <TransparentBackgroundBox sx={{ padding: 2 }}>
        <Header data={data} />
        <Stack direction="row">
          <Box flexGrow={6} flexBasis={0} mt={8}>
            <AdditionalInfoTable data={data} />
            <Box mt={4}>
              <Typography variant="h5">
                <strong>Awards</strong> {data.Awards}
              </Typography>
            </Box>
          </Box>
          <Stack
            flexGrow={4}
            flexBasis={0}
            direction="column"
            alignItems={"center"}
            justifyContent={"center"}
          >
            <AddToFavouritesButton data={data} />
          </Stack>
        </Stack>
        <Stack direction="column" mt={8} alignItems="center">
          <Typography variant="h5">
            <strong>Poster</strong>
          </Typography>
          <img src={data.Poster} alt="Poster" />
        </Stack>
      </TransparentBackgroundBox>
    </PaperWithBackground>
  );
}
