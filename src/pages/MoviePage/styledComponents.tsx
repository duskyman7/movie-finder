import styled from "@emotion/styled";
import StarIcon from "@mui/icons-material/Star";
import StarOutlineIcon from "@mui/icons-material/StarOutline";
import { Box, Paper } from "@mui/material";
import AnimatedCard from "../../components/styled/AnimatedCard";

export const DividerCircle = styled.div`
  background-color: black;
  width: 8px;
  height: 8px;
  border-radius: 4px;
  margin: 0px 10px;
`;

export const PaperWithBackground = styled(Paper, {
  shouldForwardProp: (prop) => prop !== "backgroundImageUrl",
})<{ backgroundImageUrl: string }>`
  background-image: url(${(props) => props.backgroundImageUrl});
  background-repeat: no-repeat;
  background-size: cover;
`;

export const TablePaper = styled(Paper)`
  background-color: #ffffffc0;
`;

export const TransparentBackgroundBox = styled(Box)`
  background-color: #ffffffd0;
`;

export const TransparentCard = styled(AnimatedCard)`
  background-color: #ffffffc0;
`;

export const StyledOutlineIcon = styled(StarOutlineIcon)`
  left: 0;
  position: absolute;
  color: black;
`;

export const StyledStarIcon = styled(StarIcon)<{ isAdded: boolean }>`
  color: ${(props) => (props.isAdded ? "gold" : "white")};
`;

export const RelativeBox = styled(Box)`
  position: relative;
`;
