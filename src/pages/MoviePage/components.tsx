import {
  Chip,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import { useContext } from "react";
import { MovieDetail } from "../../api/models";
import { FavouritesContext } from "../../context/FavouritesContext";
import {
  DividerCircle,
  RelativeBox,
  StyledOutlineIcon,
  StyledStarIcon,
  TablePaper,
  TransparentCard,
} from "./styledComponents";

type PropsWithMovieDetail = { data: MovieDetail };

export function Header({ data }: PropsWithMovieDetail) {
  const labels = data.Genre.split(", ");
  return (
    <Stack direction={"row"} justifyContent="space-between" mb={2}>
      <Stack direction={"row"}>
        <Box>
          <Typography variant="h3">{data.Title}</Typography>
          <Stack
            direction={"row"}
            alignItems={"center"}
            divider={<DividerCircle />}
          >
            <Typography>{data.Year}</Typography>
            <Typography>{data.Rated}</Typography>
            <Typography>{data.Runtime}</Typography>
          </Stack>
          <Stack direction="row" spacing={1}>
            {labels.map((label) => (
              <Chip key={label} variant="outlined" label={label} />
            ))}
          </Stack>
        </Box>
      </Stack>
      <Stack direction="column" flexShrink={0}>
        <Typography variant="body2">IMDb RATING</Typography>
        <Typography variant="h6">
          <strong>{data.imdbRating}</strong> / 10
        </Typography>
        <Typography variant="body2">
          <small>{data.imdbVotes} votes</small>
        </Typography>
      </Stack>
    </Stack>
  );
}

export function TableData({ children }: React.PropsWithChildren) {
  return (
    <TableRow>
      <TableCell>
        <Typography>{children}</Typography>
      </TableCell>
    </TableRow>
  );
}

export function AdditionalInfoTable({ data }: PropsWithMovieDetail) {
  return (
    <Table component={TablePaper}>
      <TableBody>
        <TableData>{data.Plot}</TableData>
        <TableData>
          <strong>Director</strong> {data.Director}
        </TableData>
        <TableData>
          <strong>Writer</strong> {data.Writer}
        </TableData>
        <TableData>
          <strong>Stars</strong> {data.Actors}
        </TableData>
      </TableBody>
    </Table>
  );
}

export function AddToFavouritesButton({ data }: PropsWithMovieDetail) {
  const { favourites, addFavourite, removeFavourite } =
    useContext(FavouritesContext);
  const isAdded = !!favourites.find(({ imdbID }) => data.imdbID === imdbID);
  const onClick = () => {
    if (isAdded) {
      removeFavourite(data.imdbID);
    } else {
      addFavourite(data);
    }
  };
  return (
    <TransparentCard onClick={onClick} sx={{ padding: 2, margin: 2 }}>
      <Stack direction="column" alignItems={"center"}>
        <RelativeBox>
          <StyledStarIcon
            isAdded={isAdded}
            sx={{
              fontSize: 40,
            }}
          />
          <StyledOutlineIcon sx={{ fontSize: 40 }} />
        </RelativeBox>
        {isAdded ? (
          <>
            <Typography>Added to</Typography>
            <Typography>favourites</Typography>
          </>
        ) : (
          <>
            <Typography>Add to</Typography>
            <Typography>favourites</Typography>
          </>
        )}
      </Stack>
    </TransparentCard>
  );
}
