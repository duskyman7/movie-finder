import MenuIcon from "@mui/icons-material/Menu";
import {
  AppBar,
  Box,
  Button,
  Container,
  IconButton,
  Menu,
  MenuItem,
  Stack,
  Toolbar,
  Typography,
} from "@mui/material";
import { useCallback, useContext, useMemo, useState } from "react";
import {
  Outlet,
  redirect,
  ScrollRestoration,
  useNavigate,
} from "react-router-dom";
import { ApiKeyContext } from "../context/ApiKeyContextProvider";
import useNavigateToHome from "../hooks/useNavigateToHome";

export default function RootPage() {
  return (
    <>
      <ScrollRestoration
        getKey={(location) => {
          return location.pathname;
        }}
      />
      <AppBar position="sticky">
        <Container>
          <Toolbar>
            <Typography variant="h4">Movie finder</Typography>
            <Stack
              direction="row"
              justifyContent="flex-end"
              flexGrow={1}
              spacing={2}
            >
              <ExpandedButtons />
              <CollapsedMenu />
            </Stack>
          </Toolbar>
        </Container>
      </AppBar>
      <Container fixed>
        <Box sx={{ marginTop: 4 }}>
          <Outlet />
        </Box>
      </Container>
    </>
  );
}

function useButtonsData() {
  const { setApiKey } = useContext(ApiKeyContext);
  const navigate = useNavigate();
  const navigateToHome = useNavigateToHome();
  const onLogout = useCallback(() => {
    setApiKey(null);
    redirect("/login");
  }, [setApiKey]);
  return useMemo(
    () => [
      {
        title: "Home",
        onClick: navigateToHome,
      },
      {
        title: "Favourites",
        onClick: () => navigate("/favourites"),
      },
      {
        title: "Logout",
        onClick: onLogout,
      },
    ],
    [navigate, navigateToHome, onLogout]
  );
}

function ExpandedButtons() {
  const buttonsData = useButtonsData();

  return (
    <Stack
      direction="row"
      sx={{ display: { xs: "none", md: "flex" } }}
      spacing={2}
    >
      {buttonsData.map(({ title, onClick }) => (
        <Button key={title} color="inherit" onClick={onClick}>
          {title}
        </Button>
      ))}
    </Stack>
  );
}

function CollapsedMenu() {
  const buttonsData = useButtonsData();
  const [anchorElement, setAnchorElement] = useState<null | HTMLElement>(null);
  const onMenuClick = useCallback(
    (fn: () => void) => () => {
      setAnchorElement(null);
      fn();
    },
    []
  );

  return (
    <Box sx={{ display: { xs: "flex", md: "none" } }}>
      <IconButton
        color="inherit"
        onClick={(e) => setAnchorElement(e.currentTarget)}
      >
        <MenuIcon />
      </IconButton>
      <Menu
        id="navMenu"
        color="inherit"
        onClose={() => setAnchorElement(null)}
        anchorEl={anchorElement}
        open={anchorElement !== null}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
      >
        {buttonsData.map(({ title, onClick }) => (
          <MenuItem key={title} onClick={onMenuClick(onClick)}>
            <Typography>{title}</Typography>
          </MenuItem>
        ))}
      </Menu>
    </Box>
  );
}
