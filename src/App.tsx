import { css, Global } from "@emotion/react";
import React from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import AppRouter from "./AppRouter";
import { ApiKeyContextProvider } from "./context/ApiKeyContextProvider";
import { FavouritesContextProvider } from "./context/FavouritesContext";
import { SearchContextProvider } from "./context/SearchContext";

const queryClient = new QueryClient();

function App() {
  return (
    <React.StrictMode>
      <Global
        styles={css`
          body {
            margin: 0;
            background-color: #f0f0f0;
          }
        `}
      />
      <QueryClientProvider client={queryClient}>
        <ApiKeyContextProvider>
          <FavouritesContextProvider>
            <SearchContextProvider>
              <AppRouter />
            </SearchContextProvider>
          </FavouritesContextProvider>
        </ApiKeyContextProvider>
      </QueryClientProvider>
    </React.StrictMode>
  );
}

export default App;
