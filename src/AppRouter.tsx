import { useContext, useMemo } from "react";
import {
  createBrowserRouter,
  redirect,
  RouteObject,
  RouterProvider,
} from "react-router-dom";
import { ApiKeyContext } from "./context/ApiKeyContextProvider";
import { useHomeUrl } from "./hooks/useNavigateToHome";
import FavouritesPage from "./pages/FavouritesPage";
import HomePage from "./pages/HomePage";
import LoginPage from "./pages/LoginPage";
import MoviePage from "./pages/MoviePage";
import RootPage from "./pages/RootPage";

export default function AppRouter() {
  const homeUrl = useHomeUrl();

  const withApiKeyRouter: RouteObject[] = useMemo(
    () => [
      {
        path: "/",
        element: <RootPage />,
        children: [
          {
            path: "/",
            element: <HomePage />,
          },
          {
            path: "movie/:id",
            element: <MoviePage />,
          },
          {
            path: "favourites",
            element: <FavouritesPage />,
          },
          {
            path: "*",
            element: null,
            loader: () => redirect(homeUrl),
          },
        ],
      },
    ],
    [homeUrl]
  );

  const withoutApiKeyRouter: RouteObject[] = useMemo(
    () => [
      {
        path: "/login",
        element: <LoginPage />,
      },
      {
        path: "*",
        element: null,
        loader: () => redirect("/login"),
      },
    ],
    []
  );

  const { apiKey } = useContext(ApiKeyContext);
  const router = createBrowserRouter(
    apiKey === null ? withoutApiKeyRouter : withApiKeyRouter
  );

  return <RouterProvider router={router} />;
}
