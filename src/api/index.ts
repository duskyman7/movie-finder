import { milliseconds } from "date-fns";
import { useCallback, useContext, useMemo } from "react";
import { useInfiniteQuery, useQuery } from "react-query";
import { useNavigate } from "react-router-dom";
import { ApiKeyContext } from "../context/ApiKeyContextProvider";
import { MaybeErrorResponse, MovieDetail, SearchResponse } from "./models";

enum apiNames {
  search = "search",
  movie = "movie",
}

function useApi() {
  const context = useContext(ApiKeyContext);
  const apiKey = context.apiKey ?? "";
  const urls = useMemo(
    () =>
      ({
        [apiNames.search]:
          "http://www.omdbapi.com/?apikey=[yourkey]&s=[searchvalue]&page=[page]",
        [apiNames.movie]: "http://www.omdbapi.com/?apikey=[yourkey]&i=[id]",
      } as const),
    []
  );
  const replaceApiKey = useCallback(
    (url: string) => url.replace("[yourkey]", apiKey),
    [apiKey]
  );
  return useMemo(
    () =>
      Object.entries(urls).reduce(
        (result, [key, value]) => ({ ...result, [key]: replaceApiKey(value) }),
        {}
      ) as Record<apiNames, string>,
    [replaceApiKey, urls]
  );
}

export function useSearchMovies(searchValue: string) {
  const { apiKey } = useContext(ApiKeyContext);
  const api = useApi();
  const navigate = useNavigate();
  const apiName = apiNames.search;
  return useInfiniteQuery<SearchResponse, Error>(
    [apiKey, apiName, searchValue],
    async ({ pageParam = 1 }) => {
      const url = api[apiName]
        .replace("[searchvalue]", searchValue)
        .replace("[page]", `${pageParam}`);
      const response = await fetch(url, {
        method: "GET",
      });
      if (!response.ok) {
        throw Error(`Http error (${response.status})`);
      }
      const data =
        (await response.json()) as MaybeErrorResponse<SearchResponse>;
      if (data.Response === "False") {
        throw Error(data.Error);
      }
      return data;
    },
    {
      retry: false,
      enabled: false,
      staleTime: milliseconds({ hours: 1 }),
      cacheTime: milliseconds({ hours: 1 }),
      getNextPageParam: (lastPage, allPages) => {
        // NOTE: The API does not gives us explicit answer that there is no more
        //       data until we ask for page out of range (which is already too
        //       late). Therefore we work with the `totalResults` value and hope
        //       that the page size never changes
        const PAGE_SIZE = 10; // from observation
        return allPages.length * PAGE_SIZE < Number(lastPage.totalResults)
          ? allPages.length + 1
          : undefined;
      },
      onSuccess: () => {
        navigate(`/?search=${searchValue}`); // update the url so the searchValue is not lost upon refresh (data will be lost though).
      },
    }
  );
}

export function useGetMovieDetails(movieId: string) {
  const { apiKey } = useContext(ApiKeyContext);
  const api = useApi();
  const apiName = apiNames.movie;
  return useQuery<MovieDetail, Error>([apiKey, apiName, movieId], async () => {
    const url = api[apiName].replace("[id]", `${movieId}`);
    const response = await fetch(url);
    if (!response.ok) {
      throw Error(`Http error (${response.status})`);
    }
    const data = (await response.json()) as MaybeErrorResponse<MovieDetail>;
    if (data.Response === "False") {
      throw Error(data.Error);
    }
    return data;
  });
}
