import { useCallback, useContext, useMemo } from "react";
import { useNavigate } from "react-router-dom";
import { SearchContext } from "../context/SearchContext";

export function useHomeUrl() {
  const { searchValue } = useContext(SearchContext);
  return useMemo(() => `/?search=${searchValue}`, [searchValue]);
}

export default function useNavigateToHome() {
  const navigate = useNavigate();
  const url = useHomeUrl();
  return useCallback(() => navigate(url), [navigate, url]);
}
