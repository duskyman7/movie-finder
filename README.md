# Movie Finder

App is deployed on DigitalOcean droplet: http://duskyman.duckdns.org:3000/

Technologies used:

- **yarn** as package manager
- **react-query** for data-handling
- **Material UI** as layout and component library
- **Emotion** for additional custom styling

## Scripts in package.json

- `yarn start` runs development server
- `yarn build` bundles optimized production build (which is served on the droplet)

## Intentional deviations from assignment

### Login page

- Additional login page is created. No other pages can be accessed until the user inputs API key that will be used to fetch data
- API key for [OMDb](https://www.omdbapi.com/) is stored in localstorage and serves as a "login" credential. The key is provided in Coderbyte submission, or you can always [ask for new key on their page](https://www.omdbapi.com/apikey.aspx)

### Search page

- There is no search button as the search is launched automatically after user stops typing (1 sec delay).

### Movie detail

- The "like button" is not next to the title. It's a bit lower.
- Not 100% data provided by OMDb is exhausted (the detail screen still provides more details than the search list).

## Suggestions for improvements

- The fetched information is lost when the url is manually entered.
- obviously design could be better

# I hope you like it!
